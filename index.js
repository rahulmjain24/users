const container = document.querySelector('.container')
const loader = document.querySelector('.lds-ring')

const URL = 'https://fakestoreapi.com/users'

function createError(message) {
    const h1 = document.createElement('h1')
    
    h1.className = 'error'
    h1.textContent = message

    return h1
}

function createUser(user) {
    const userDiv = document.createElement('div')
    const names = document.createElement('div')
    const name = document.createElement('p')
    const username = document.createElement('p')
    const contact = document.createElement('div')
    const email = document.createElement('p')
    const phone = document.createElement('p')
    const address = document.createElement('p')

    userDiv.classList.add('user')

    names.classList.add('names')

    name.classList.add('name')
    name.appendChild(document.createTextNode(`${user.name.firstname} ${user.name.lastname}`))

    username.classList.add('username')
    username.appendChild(document.createTextNode(`@${user.username}`))

    names.appendChild(name)
    names.appendChild(username)

    contact.classList.add('contact')

    email.classList.add('email')
    email.appendChild(document.createTextNode(`Email: ${user.email}`))

    phone.classList.add('phone')
    phone.appendChild(document.createTextNode(`Phone: ${user.phone}`))

    contact.appendChild(email)
    contact.appendChild(phone)

    address.classList.add('address')
    address.appendChild(document.createTextNode(`Address: ${user.address.street} ${user.address.number}, ${user.address.city}, ${user.address.zipcode}`))

    userDiv.appendChild(names)
    userDiv.appendChild(contact)
    userDiv.appendChild(address)

    return userDiv
}

fetch(URL)
    .then((response) => {
        return response.json()
    })
    .then((users) => {
        console.log(users)
        container.removeChild(loader)
        if(users.length === 0) {
            container.appendChild(createError('No Users!!!'))
        } else {
            users.forEach((user) => {
                container.appendChild(createUser(user))
            })
        }
    })
    .catch((err) => {
        container.removeChild(loader)
        container.appendChild(createError('Something went wrong!!!'))
        console.error(err)
    })